# uniapp-uView-Base

#### 介绍
基于 uView1.8.6 封装的一套基础模版，开箱即用，可直接开始快速开发业务需求～

```
想用uview2.0版本的去uview2的分支上下载
```

## 项目地址
- [小程序前端 UniApp 项目地址](https://gitee.com/my_hujinchen/uniapp-u-view-base)
- [后台前端 Vue 项目地址](https://gitee.com/my_hujinchen/catchdmin_vue)
- [后端 PHP 项目地址](https://gitee.com/my_hujinchen/catchAdmin)
- 这3个都是配套使用的，可以基于这基础上开发。
